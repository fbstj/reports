<?php
require_once 'lib/db.php';
require_once 'lib/reports.php';
require_once 'lib/entries.php';
require_once 'lib/urls.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<title>EDITOR</title>
<?php require_once 'lib/nav.inc.php'; ?>
<?php

if (!isset($_GET['type']) || !isset($_GET['id']))
    die("can't edit without type & id");

$type = $_GET['type'];
$id = $_GET['id'];

if ($type == 'entry')
{
    $e = \entries\get($id);
    if ($e == null)
        die("$id isn't an entry");
    $e_url = \urls\get($e->url);
    if (isset($e->date))
        $e_date = \db\get_claim($e->date);
    if (isset($e->note))
        $e->note = \db\get_claim($e->note)->body;

    if (count($_POST))
    {   // save
        $obj = (object)$_POST;
        $old = [];
        if ($e->body != $obj->body)
        {
            $old['body'] = $e->body;
            \db\set_claim($id, $obj->body, 'entry');
            # TODO: history
        }
        if ($e_url->href != $obj->url)
        {
            $old['url'] = $e_url->href;
            $url = \db\get_children($id, 'url')[0];
            \db\set_claim($url, $obj->url, 'url');
        }
        if ($e_url->text != $obj->cite)
        {
            $old['urltext'] = $e_url->text;
            \urls\update_text($e->url, $obj->cite);
        }
        if (isset($e_date))
        {
            $old['date'] = $e_date->body;
            $date = \db\get_children($id, 'date')[0];
            if ($obj->date != '')
                \db\set_claim($date, $obj->date, 'date');
            else
                \db\del_claim($date, 'date');
        }
        else if ($obj->date != '')
        {
            \db\add_claim($obj->date, 'date', $id);
        }
        if ($obj->note != $e->note)
        {
            $old['note'] = $e->note;
            \db\update_child($id, 'note', $obj->note);
        }
        if (count($old))
        {   # $old contains history data
            \db\set_child_claim(time(), 'saved', $id);
        }
        if (isset($_GET['search']))
            header('Location: ./search.php?q='.$_GET['search']);
        else
            header('Location: ./find.php?id='.$id);
        die();
    }

?>
<form method=POST>
    <h1>Edit entry</h1>
        <label for="body">Body (html)</label>
    <textarea name=body required><?=$e->body?></textarea>
        <label for="url">Source url</label>
    <input name=url type=url required value="<?=$e_url->href?>">
        <label for="cite">Source url text <small>[optional]</small></label>
    <input name=cite value="<?=$e_url->text?>">
        <label for="report">Report id</label>
    <input name=report type=number required value="<?=$e->parent?>">
        <label for="date">Date <small>[optional]</small></label>
    <input name=date type=date value="<?=$e_date->body?>">
        <label for="note">Footnotes <small>[optional]</small></label>
    <textarea name=note><?=$e->note?></textarea>
    <button>Save</button>
    <a href="./find.php?id=<?=$id?>">Cancel</a>
</form>
<?php
    die();
}
if ($type == 'report')
{
    $r = \reports\get($id);
    if ($r == null)
        die("$id isn't an entry");
    $r_date = \db\get_claim($r->date)->body;

    if (count($_POST))
    {   // save
        $obj = (object)$_POST;
        $old = [];
        if ($r->body != $obj->title)
        {
            $old['body'] = $r->body;
            \db\set_claim($id, $obj->title, 'report');
        }
        if ($r_date != $obj->date)
        {
            $old['date'] = $r_date;
            $date = \db\get_children($id, 'date')[0];
            \db\set_claim($date, $obj->date, 'date');
        }
        if ($obj->meta == '' && isset($r->meta))
        {
            $old['meta'] = \db\get_claim($r->meta)->body;
            $meta = \db\get_children($id, 'meta')[0];
            \db\del_claim($meta, 'meta');
        }
        if ($r->meta != $obj->meta)
        {
            $old['meta'] = \db\get_claim($r->meta)->body;
            $meta = \db\get_children($id, 'meta');
            if (count($meta))
                \db\set_claim($meta[0], $obj->meta, 'meta');
            else
                \db\add_claim($obj->meta, 'meta', $id);
        }
        if (count($old))
        {   # $old contains history data
            \db\set_child_claim(time(), 'saved', $id);
        }
        if (isset($_GET['list']))
            header('Location: ./list.php');
        else
            header('Location: ./find.php?id='.$id);
        die();
    }
?>
<form method=POST>
    <h1>Edit report</h1>
        <label for="title">Title</label>
    <input name=title required value="<?=$r->body?>">
        <label for="date">Date</label>
    <input name=date type=date value="<?=$r_date?>">
        <label for="meta">Meta (html)</label>
    <textarea name=meta><?=$r->meta?></textarea>
    <button>Save</button>
</form>
<?php
    die();
}
