<?php
require_once 'lib/db.php';
require_once 'lib/reports.php';
require_once 'lib/entries.php';
require_once 'lib/urls.php';
require_once 'lib/dates.php';
require_once 'lib/tags.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<title>Activity</title>
<?php require_once 'lib/nav.inc.php'; ?>
<header>
    <h1>Activity</h1>
</header>

<ul>
<?php

$WHERE = "`type` = 'saved' ORDER BY `body` DESC LIMIT 10";
foreach (\db\select_column(\db\COL_ID, $WHERE) as $id)
{
    $obj = \db\get_claim($id);
    $id = $obj->parent;
    $changed = \dates\relative_time($obj->body);
    $changed_name = date('r', $obj->body);
    $changed = "<time title='{$changed_name}' datetime='{$obj->body}'>{$changed}</time>";
    $obj = \db\get_claim($id);
    
?>
    <hr>
    <li id="<?=$id?>">
        <small>[<a href="./find.php?id=<?=$id?>"><?=$obj->type?></a>]<br>[<?=$changed?>]</small>
        <?=$obj->body;?>
        <div>&nbsp;<?=\tags\show_all($id)?></div>
    </li>
<?php
}

?>
</ul>
