<?php
require_once 'lib/entries.php';
require_once 'lib/urls.php';
require_once 'lib/dates.php';
require_once 'lib/tags.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<title>Search - <?=$_GET['q']?></title>
<?php require_once 'lib/nav.inc.php'; ?>
<header>
<h1>Search</h1>
<form method=GET>
    <input name=q value="<?=$_GET['q']?>" />
    <button>Search</button>
</form>
<?php

$q = $_GET['q'];

if (is_null($q))
{
    die ();
}

?>
</header>

<ul>
<?php

foreach (\entries\search($q) as $e_id)
{
?>
    <hr>
    <li id="<?=$e_id?>">
        <small>[<?=join(']<br>[', \entries\get_links($e_id))?>]</small>
        <?=\entries\show($e_id, $opts)?>
        <div><?=\tags\show_all($e_id)?></div>
    </li>
<?php
}

?>
</ul>
