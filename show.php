<?php
require_once 'lib/reports.php';
require_once 'lib/entries.php';
require_once 'lib/urls.php';
require_once 'lib/dates.php';
require_once 'lib/tags.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<?php

if (!isset($_GET['r']))
{   // redirect to ./list.php
    header('Location: ./list.php');
    die();
}

$report = \reports\get($_GET['r']);
if ($report == null)
{   // redirect to ./list.php
    header('Location: ./find.php?id='.$_GET['r']);
    die();
}

?>
<title><?=$report->body?></title>
<?php require_once 'lib/nav.inc.php'; ?>
<header>
    <?php
        $urls = \urls\_generate($report->id, 'report');
        unset($urls['cite']);
        $links = [];
        foreach ($urls as $u)
            $links[] = $u->show();
    ?>
    <small>[<?=join(']<br>[', $links)?>]</small>
    <h1><?=$report->body?></h1>
    <p><?=\dates\get($report->date)->show()?></p>
    <div><?=$report->meta?></div>
    <div>&nbsp;<?=\tags\show_all($report->id)?></div>
</header>
<ul>
<?php

foreach (\entries\from_report($report->id) as $e_id)
{
?>
    <hr>
    <li id="<?=$e_id?>">
        <small>[<?=join(']<br>[', \entries\get_links($e_id))?>]</small>
        <?=\entries\show($e_id);?>
        <div><?=\tags\show_all($e_id)?></div>
    </li>
<?php
}

?>
</ul>

<nav>
    <a href="./add.php?entry&r=<?=$report->id?>">Add new entry</a>
    ·
    <a href="./add_tweet.php?report=<?=$report->id?>">Add new tweet</a>
</nav>
<!--
<script>
    function show_add_form()
    {
        var el = document.createElement('iframe');
        el.width = '100%;';
        el.height = '100%';
        el.frameborder = 0;
        el.src = './add.php?entry&r=<?=$report->id?>';
        document.body.appendChild(el);
    }
</script>
<button onclick="show_add_form()">Add new entry</button>
-->
