<?php
require_once 'lib/db.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<title>EDITOR</title>
<?php require_once 'lib/nav.inc.php'; ?>
<?php

$r_action = '?report';
$r_button = 'Add';
$e_action = '?entry';
$e_button = 'Add';

if (array_key_exists('report', $_GET))
{
    if (count($_POST))
    {   // insert
        $obj = (object)$_POST;
        $id = \db\add_claim($obj->title, 'report');
        $date = \db\add_claim($obj->date, 'date', $id);
        if ($obj->meta != '')
            \db\add_claim($obj->meta, 'meta', $id);
        \db\add_claim(time(), 'saved', $id);
        header('Location: find.php?id='.$id);
        die();
    }
?>
<form method=POST>
    <h1>Add report</h1>
        <label for="title">Title</label>
    <input name=title required>
        <label for="date">Date</label>
    <input name=date type=date>
        <label for="meta">Meta (html)</label>
    <textarea name=meta></textarea>
    <button>Add</button>
</form>
<?php
    
}
else if (array_key_exists('entry', $_GET))
{
    if (count($_POST))
    {   // insert
        $obj = (object)$_POST;
        $id = \db\add_claim($obj->body, 'entry', $obj->report);
        $url = \db\add_claim($obj->url, 'url', $id);
        if ($obj->cite != '')
            \db\add_claim($obj->cite, 'urltext', $url);
        if ($obj->date != '')
            \db\add_claim($obj->date, 'date', $id);
        if ($obj->note != '')
            \db\add_claim($obj->note, 'note', $id);
        \db\add_claim(time(), 'saved', $id);
        header('Location: find.php?id='.$id);
        die();
    }
    $r = $_GET['r'];
?>
<form method=POST>
    <h1>Add entry</h1>
        <label for="body">Body (html)</label>
    <textarea name=body required></textarea>
        <label for="url">Source url</label>
    <input name=url type=url required>
        <label for="cite">Source link text <small>[optional]</small></label>
    <input name=cite>
        <label for="report">Report id</label>
    <input name=report type=number required value="<?=$r?>">
        <label for="date">Date <small>[optional]</small></label>
    <input name=date type=date>
        <label for="note">Footnotes <small>[optional]</small></label>
    <textarea name=note></textarea>
    <button>Add</button>
</form>
<?php

}
