<?php
require_once 'lib/entries.php';
require_once 'lib/urls.php';
require_once 'lib/dates.php';
require_once 'lib/tags.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<title>List all Tags</title>
<?php require_once 'lib/nav.inc.php'; ?>
<?php
if (array_key_exists('tag', $_GET))
{
    $t = $_GET['tag'];
?>
<header>
    <h1>Entries tagged #<?=$t?></h1>
</header>
<ul>
<?php
foreach (\tags\parents($t) as $id)
{
    $obj = \db\get_claim($id);
?>
    <hr>
    <li id="<?=$id?>">
        <small>[<a href="./find.php?id=<?=$id?>"><?=$obj->type?></a>]</small>
        <?=$obj->body?>
        <div><?=\tags\show_all($id)?></div>
    </li>
<?php
    }
?>
</ul>
<?php
    die();
}
?>
<header>
    <h1>Tags</h1>
</header>
<ul>
<?php
foreach (\tags\all() as $tag)
{
?>
    <li><?=\tags\show($tag)?> - <?=count(\tags\parents($tag))?></li>
<?php
}
?>
</ul>
