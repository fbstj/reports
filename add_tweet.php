<?php
require_once './lib/twitter.api.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<title>ADD TWEETS</title>
<?php require_once 'lib/nav.inc.php'; ?>
<?php

/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = json_decode(file_get_contents("./twitter.json"), true);

$twitter = new \TwitterAPIExchange($settings);

function user_tweets($user, $b4=null)
{
    global $twitter;
    /** Perform a GET request and echo the response **/
    /** Note: Set the GET field BEFORE calling buildOauth(); **/
    $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    $getfield = '?screen_name='.$user.'&include_rts=false'.($b4?'&max_id='.$b4:'');
    $requestMethod = 'GET';
    $res = $twitter->setGetfield($getfield)
                 ->buildOauth($url, $requestMethod)
                 ->performRequest();
    return json_decode($res);
}

function tweet($id)
{
    global $twitter;
    /** Perform a GET request and echo the response **/
    /** Note: Set the GET field BEFORE calling buildOauth(); **/
    $url = 'https://api.twitter.com/1.1/statuses/show.json';
    $getfield = '?id='.$id;
    $requestMethod = 'GET';
    $res = $twitter->setGetfield($getfield)
                 ->buildOauth($url, $requestMethod)
                 ->performRequest();
    return json_decode($res);
}

$conv = [];
$id = $_GET['tweet'];

if (!is_numeric($id))
{
?>
<form method=GET>
    <h1>Find tweet</h1>
    <input name=report type=hidden value="<?=$_GET['report']?>">
        <label for=tweet>Tweet ID</label>
    <input name=tweet type=number required />
    <button>Load</button>
</form>
<?php
    die();
}

while ($id != null)
{
    $tweet = tweet($id);
    $conv[] = $tweet;
    $id = $tweet->in_reply_to_status_id;
}

$conv = array_reverse($conv);

ob_start();
foreach($conv as $tweet)
{
    $url = "https://twitter.com/{$tweet->user->screen_name}/status/{$tweet->id}"
?>
<b><a href="<?=$url?>"><?=$tweet->user->name?></a></b>
<p><?=$tweet->text?></p>

<?php
}
$out = ob_get_contents();
ob_end_clean();

?>
<form method=POST action="./add.php?entry" style="float: right;">
    <h1>Save tweet</h1>
        <label for="body">Body (html)</label>
    <textarea name=body required><?=$out?></textarea>
        <label for="url">Cite url</label>
    <input name=url type=url required value="<?=$url?>">
        <label for="cite">Cite text <small>[optional]</small></label>
    <input name=cite value="Twitter">
        <label for="report">Report id</label>
    <input name=report type=number required value="<?=$_GET['report']?>">
        <label for="date">Date <small>[optional]</small></label>
    <input name=date type=date value="<?=date('Y-m-d', strtotime($tweet->created_at))?>">
        <label for="note">Footnotes <small>[optional]</small></label>
    <textarea name=note></textarea>
    <button>Add</button>
</form>


<div><?=$out?></div>
