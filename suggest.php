<?php
# suggestion management
require_once 'lib/config.php';
require_once 'lib/db.php';

/*
    each suggestion has a name, link body (html) and a edited date 
    optionally:
        - set of entry ids
        - report id(s?)
        - completed date
        - tags?
*/

function get_suggestion($id)
{
    $root = \db\get_claim($id);
    if ($root->type != 'suggestion')
    {
        return false;
    }
    $row = (object)[];
    $row->name = $root->body;
    $url = \db\get_children($id, 'url');
    $url = \db\get_claim($url[0]);
    $row->url = $url->body;
    $body = \db\get_children($id, 'body');
    $body = \db\get_claim($body[0]);
    $row->body = $body->body;
    $stamp = \db\get_children($id, 'saved');
    $row->stamp = \db\get_claim($stamp[0])->body;
    #optional
    if (!is_null($root->parent))
        $row->report = $root->parent;
    $row->entries = [];
    $row->tags = [];
    $done = \db\get_children($id, 'date');
    if (count($done) > 0)
        $row->done = \db\get_claim($done[0])->body;
    return $row;
}

if (isset($_GET['id']))
{   # try get deets
    define('ID', $_GET['id']);
    $row = get_suggestion(ID);
    if ($row === false)
    {   # go to find.php
        header('Location: '. BASE .'/find.php?id='. ID);
        return;
    }
}

?>
<meta charset="utf-8">
<link rel="stylesheet" href="<?=BASE?>/style.css" type="text/css" />
<title>Suggestions</title>
<?php require_once 'lib/nav.inc.php'; ?>

<?php

if (ROUTE == '/add')
{   # show add form
    $route = route('set', [ 'goto' => OLD_ROUTE ]);
?>
<form method=POST action='<?=$route?>'>
    <h1>Add suggestion</h1>
        <label for="title">Title</label>
    <input name=title required>
        <label for="url">Link</label>
    <input name=url type=url pattern="https?://.+" required>
        <label for="body">Body</label>
    <textarea name=body required></textarea>
        <label for="report">Report (id)</label>
    <input name=report pattern="\d+">
        <label for="entries">Entries</label>
    <input name=entries pattern="\d+(,\d+)*">
        <label for="tags">Tags</label>
    <input name=tags pattern="[^,]+(,[^,]+)*">
        <label for="done">Completed on</label>
    <input name=done type=date>
    <button>Add</button>
</form>
<?php
    return;
}
else if (ROUTE == '/set')
{   # POST result
    if (defined('ID'))
    {   # update
        print 'update';
        $report = $_POST['report'];
        \db\set_parent(ID, 'suggestion', $report == '' ? null : $report);
    }
    else
    {   # insert
        print 'insert';
        if ($_POST['report'] != '')
            $report = $_POST['report'];
        $id = \db\add_claim($_POST['title'], 'suggestion', $report);
        define('ID', $id);
        \db\update_child(ID, 'url', $_POST['url']);
    }
    \db\update_child(ID, 'body', $_POST['body']);
    # add date
    \db\update_child(ID, 'saved', time());
    \db\update_child(ID, 'date', $_POST['done'] == '' ? null : $_POST['done']);
    # update entries (what type@??) tags
    # redirect to OLD_ROUTE
    header('Location: '. OLD_ROUTE);
    return;
}
else if (ROUTE == '/edit' and defined('ID'))
{   # /edit?id=
    $route = route('set', [ 'id' => ID,  'goto' => OLD_ROUTE ]);
?>
<form method=POST action="<?=$route?>">
    <h1>Change suggestion:</h1>
    <div>
        <b>Suggestion:</b>
        <a href="<?=$row->url?>"><?=$row->name?></a>
    </div>
        <label for="body">Body</label>
    <textarea name=body required><?=$row->body?></textarea>
        <label for="report">Report (id)</label>
    <input name=report pattern="\d+" value="<?=$row->report?>">
        <label for="entries">Entries</label>
    <input name=entries pattern="\d+(,\d+)*" value="<?=join(',', $row->entries)?>">
        <label for="tags">Tags</label>
    <input name=tags pattern="[^,]+(,[^,]+)*" value="<?=join(',', $row->tags)?>">
        <label for="done">Completed on</label>
    <input name=done type=date>
    <button>Save</button>
</form>
<?php
    return;
}
else if (ROUTE == '/show' and defined('ID'))
{   # /show?id=
    $edit = route('edit', [ 'id' => ID, 'goto' => THIS_ROUTE ]);
    if (!is_null($row->report))
    {
        $report = \db\get_claim($row->report);
        $report->url = route('', [ 'r' => $row->report ], BASE.'/show.php');
    }
?>
<section>
    <h1><a href="<?=$row->url?>"><?=$row->name?></a></h1>
    <p>
        <i>Edited:</i> <?=$row->stamp?> <a href="<?=$edit?>">[edit]</a>,
        <i>Completed:</i> <?=$row->done?>,
        <i>Report:</i> <a href="<?=$report->url?>"><?=$report->body?></a>
    </p>
    <div>
        <?=$row->body?>
    </div>
</section>
        <b>Suggestion:</b>
        <a href="<?=$row->link?>"><?=$row->name?></a>
<?php
    # TODO: show tags, entries
?>
<section>
    
</section>
<?php    
    return;
}
else if (ROUTE == '/tags')
{
    return;
}
# ELSE /show

$WHERE = "`type` = 'suggestion'";
if (isset($_GET['q']))
{   # /find
    $WHERE .= 'AND `body` LIKE %'. $_GET['q'] .'%';
}
else if (isset($_GET['tag']))
{   # /tagged
}
else # /all
{
}
?>
<header>
    <h1>Suggestions</h1>
    <a href="<?=route('add', [ 'goto' => THIS_ROUTE ])?>">Add</a>
</header>
<table>
    <tr>
        <th>Suggestion</th>
        <th>For</th>
        <th>Edited</th>
        <th>Completed</th>
    </tr>
<?php
foreach (\db\select_column(\db\COL_ID, $WHERE) as $id)
{
    $row = get_suggestion($id);
    $show = route('show', [ 'id' => $id ]);
    $edit = route('edit', [ 'id' => $id, 'goto' => THIS_ROUTE ]);
?>
    <tr>
        <td>
            <a href="<?=$show?>"><?=$row->name?></a>
            <a href="<?=$edit?>">[edit]</a>
        </td>
        <td>
<?php
    if(!is_null($row->report))
    {
        $report = route('', [ 'r' => $row->report ], BASE.'/show.php');
?>
            <a href="<?=$report?>">report</a>,
<?php } ?>
            <a href="<?=$row->url?>">source</a>
        </td>
        <td><?=$row->stamp?></td>
        <td><?=$row->done?></td>
    </tr>
<?php
}

?>
</ul>
