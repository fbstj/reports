<?php
require_once 'lib/reports.php';
require_once 'lib/entries.php';
require_once 'lib/urls.php';
require_once 'lib/dates.php';
require_once 'lib/tags.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<title>EDIT TAGS</title>
<?php require_once 'lib/nav.inc.php'; ?>
<?php

if (!array_key_exists('id', $_GET))
{
    header('Location: ./list_tags.php');
    die();
}

$id = $_GET['id'];

if (count($_POST))
{
    $new_tags = \tags\split_tags($_POST['tags']);
    $changes = \tags\update($id, $new_tags);
    if ($changes != 0)
    {   # $old contains history data
        \db\set_child_claim(time(), 'saved', $id);
    }

    header('Location: ./find.php?id='.$id);
    die();
}

$obj = \db\get_claim($id);
$tags = \tags\get($id);

?>
<h1>Edit tags</h1>
<section>
    <small>[<a href="./find.php?id=<?=$id?>"><?=$obj->type?></a>]</small>
    <?=$obj->body;?>
    <hr>
    <form method=POST>
        <input name=tags value="<?=join(',',$tags)?>">
        <button>Save tags</button>
    </form>
</section>
