<?php
namespace tags;
require_once 'lib/db.php';

const COL_BODY = "DISTINCT `body` COLLATE NOCASE";
const TYPE_WHERE = "'tag'";
const BODY_WHERE = "? COLLATE NOCASE";

function all()
{
    $tags = \db\select_column(COL_BODY, [\db\COL_TYPE => TYPE_WHERE]);
    sort($tags);
    return array_map("\strtolower", $tags);
}

function get($id)
{
    $WHERE = [\db\COL_TYPE => TYPE_WHERE, \db\COL_PARENT => \db\COL_MATCH];
    $tags = \db\select_column(COL_BODY, $WHERE, [$id]);
    sort($tags);
    return array_map("\strtolower", $tags);
}

function add($tag, $id)
{
    \db\add_claim(strtolower($tag), 'tag', $id);
}

function del($tag, $id)
{
    $WHERE = [
        \db\COL_TYPE => TYPE_WHERE,
        \db\COL_BODY => BODY_WHERE,
        \db\COL_PARENT => \db\COL_MATCH
        ];
    $tid = \db\select_one(\db\COL_ID, $WHERE, [$tag, $id]);
    \db\del_claim($tid->id, 'tag');
}

function parents($tag)
{
    $WHERE = [ \db\COL_TYPE => TYPE_WHERE, \db\COL_BODY => BODY_WHERE ];
    return \db\select_column(\db\COL_PARENT, $WHERE, [$tag]);
}

function update($parent, $new_tags)
{
    $old_tags = \tags\get($parent);
    $i = 0;
    foreach ($new_tags as $tag)
    {   // find tags to add
        if (in_array($tag, $old_tags))
            continue;
        \tags\add($tag, $parent);
        $i += 1;
    }
    foreach ($old_tags as $tag)
    {   // delete thises tags
        if (in_array($tag, $new_tags))
            continue;
        \tags\del($tag, $parent);
        $i += 1;
    }
    return $i;
}

function show($tag)
{
    $tag = strtolower($tag);
    $url = "./list_tags.php?tag={$tag}";
    return "<a href=\"{$url}\">#{$tag}</a>";
}

function show_all($parent)
{
    $tags = \tags\get($parent);
    $tags = array_map('\tags\show', $tags);
    return join(', ', $tags) ;
}

function split_tags($tags)
{
    if (trim($tags) == '')
        return [];
    $out = [];
    foreach(split(',', $tags) as $tag)
    {
        $out[] = trim($tag);
    }
    return $out;
}
