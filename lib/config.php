<?php

define('ROUTE', $_SERVER['PATH_INFO']);
define('SELF', $_SERVER['SCRIPT_NAME']);
define('BASE', dirname(SELF));

define('THIS_ROUTE', route(ROUTE, $_GET));

if (isset($_GET['goto']))
    define('OLD_ROUTE', base64_decode($_GET['goto']));
else
    define('OLD_ROUTE', route('/all'));

function route($route, $query = null, $page = SELF)
{
    if (count($query) > 0)
    {
        if (isset($query['goto']))
            $query['goto'] = base64_encode($query['goto']);
        $query = '?'. (is_string($query) ? $query : http_build_query($query));
    }
    else
        $query = '';
    if (substr($route, 0, 1) == '/')
        $route = substr($route, 1);
    if ($route == '')
        return $page . $query;
    return $page .'/'. $route . $query;
}

