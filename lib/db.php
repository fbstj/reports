<?php
namespace db;
# library for database access
$DB = new \PDO("sqlite:./lib/iv.db");
$DB->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

function catch_error($value)
{
    global $DB;
    var_dump($DB->errorInfo());
    var_dump($value);
    die();
}

const COL_ALL = '*';
const COL_ID  = '`id`';
const COL_BODY = '`body`';
const COL_TYPE = '`type`';
const COL_PARENT = '`parent`';
const COL_MATCH = '?';

function __Q($sql, $params)
{
    global $DB;
    $q = $DB->prepare($sql);
    $q->execute($params);
    return $q;
}

function ands($args)
{
    $out = [];
    foreach($args as $key => $value)
        $out[] = "$key = $value";
    return join(' AND ', $out);
}

function add_claim($body, $type, $parent = null)
{
    global $DB;
    $q = $DB->prepare(
<<<SQL
    INSERT INTO
    Claims (`body`, `type`, `parent`)
    VALUES (     ?,      ?,        ?)
SQL
    );
    $q->execute([$body, $type, $parent]);
    return $DB->lastInsertId();
}

function set_parent($id, $type, $parent)
{
    $Q = "UPDATE Claims SET "
        .ands([ COL_PARENT => COL_MATCH ])
        ." WHERE "
        .ands([
            COL_ID => COL_MATCH,
            COL_TYPE => COL_MATCH,
            ])
        ." LIMIT 1";
    __Q($Q, [$parent, $id, $type]);
}

function set_claim($id, $body, $type)
{
    $Q = "UPDATE Claims SET "
        .ands([ COL_BODY => COL_MATCH ])
        ." WHERE "
        .ands([
            COL_ID => COL_MATCH,
            COL_TYPE => COL_MATCH,
            ])
        ." LIMIT 1";
    __Q($Q, [$body, $id, $type]);
}

function set_child_claim($body, $type, $parent)
{
    $Q = "UPDATE Claims SET "
        .ands([ COL_BODY => COL_MATCH ])
        ." WHERE "
        .ands([
            COL_TYPE => COL_MATCH,
            COL_PARENT => COL_MATCH,
            ])
        ." LIMIT 1";
    __Q($Q, [$body, $type, $parent]);
}

function del_claim($id, $type)
{
    $Q = "DELETE FROM Claims WHERE ".ands([
        COL_ID => COL_MATCH,
        COL_TYPE => COL_MATCH,
        ])
        ." LIMIT 1";
    __Q($Q, [$id, $type]);
}

function update_child($parent, $type, $value = null)
{
    $kids = \db\get_children($parent, $type);
    if (count($kids) == 0)
        \db\add_claim($value, $type, $parent);
    else if (!empty($value))
        \db\set_claim($kids[0], $value, $type);
    else
        \db\del_claim($kids[0],  $type);
}
# -- select functions

function select_column($column, $where, $params = null)
{
    if (is_array($where))
        $where = ands($where);
    $sql = "SELECT {$column} FROM Claims WHERE {$where}";
    return __Q($sql, $params)->fetchAll(\PDO::FETCH_COLUMN, 0);
}

function select_one($column, $where, $params = null)
{
    if (is_array($where))
        $where = ands($where);
    $sql = "SELECT {$column} FROM Claims WHERE {$where} LIMIT 1";
    return __Q($sql, $params)->fetch();
}

# ---- specialisations

function get_claim($id)
{
    return select_one(COL_ALL, [COL_ID => COL_MATCH], [$id]);
}

function get_all_children($id)
{
    return select_column(COL_ID, [COL_PARENT => COL_MATCH], [$id]);
}

function get_children($id, $type)
{
    return select_column(COL_ID, [COL_PARENT => COL_MATCH, COL_TYPE => COL_MATCH], [$id, $type]);
}

function list_type($type)
{
    return select_column(COL_ID, [COL_TYPE => COL_MATCH], [$type]);
}
