<?php
namespace urls;
require_once 'lib/db.php';

class URL
{
    public $href;
    public $text;
    public function show($text = null)
    {
        if (!is_null($this->text))
            $text = $this->text;
        return "<a href=\"{$this->href}\">{$text}</a>"; 
    }
    public function __construct($href, $text = null)
    {
        $this->href = $href;
        $this->text = $text;
    }
}

function get($id)
{
    $url = \db\get_claim($id);
    if ($url->type != 'url')
        return null;
    // get text
    $url_txts = \db\get_children($id, 'urltext');
    if (count($url_txts))
        $text = \db\get_claim($url_txts[0]);
    return new URL($url->body, $text->body);
}

function update_text($id, $text)
{
    return \db\update_child($id, 'urltext', $text == '' ? null : $text);
}

function _list($parent)
{
    $out = [];
    foreach (\db\get_children($parent, 'url') as $id)
        $out[] = get($id);
    return $out;
}

function _generate($id, $type)
{
    $x = [
        'cite' => "./find.php?id={$id}",
        'edit' => "./edit.php?type={$type}&id={$id}",
        'tags' => "./edit_tags.php?id={$id}",
    ];
    $y = [];
    foreach ($x as $k => $v)
        $y[$k] = new URL($v, $k);
    return $y;
}
