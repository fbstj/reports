<?php
namespace reports;
require_once "db.php";

function all($sort = true, $deep = true)
{
    $reports = [];
    foreach (\db\list_type("report") as $id)
    {
        $reports[] = get($id, $deep);
    }
    if ($sort)
    {    // sort in reverse chronologic order (ORDER BY date DESC)
        usort($reports, function($a, $b){ return $b->date->value - $a->date->value; });
    }
    return $reports;
}

function get($id, $deep = false)
{
    $report = \db\get_claim($id);
    if ($report->type != 'report')
        return null;
    $report->date = \db\get_children($id, 'date')[0];
    if ($deep)
        $report->date = \dates\get($report->date);
    $meta = \db\get_children($id, 'meta');
    if (count($meta) == 1)
        $report->meta = \db\get_claim($meta[0])->body;
    return $report;
}

function get_links($r_id)
{
    $links = [];
    foreach (\urls\_list($r_id) as $u)
        $links[] = $u->show('source');
    $d = \dates\get_from_parent($r_id);
    if (!is_null($d))
        $links['date'] = $d->show();
    foreach (\urls\_generate($r_id, 'report') as $k => $u)
    {
        $links[$k] = $u->show($k);
    }
    return $links;
}
