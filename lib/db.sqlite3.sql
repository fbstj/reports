CREATE TABLE "Reports" (
  "id" integer NOT NULL,
  "title" text NOT NULL,
  "date" integer NOT NULL,
  "body" text NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "Entries" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "body" text NOT NULL,
  "url" text NOT NULL,
  "report" integer NOT NULL,
  "date" integer NULL,
  "cite" text NULL,
  FOREIGN KEY ("report") REFERENCES "Reports" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE "Tags" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "tag" text COLLATE 'NOCASE' NOT NULL,
  "index" integer NOT NULL,
  "type" integer NOT NULL
);
