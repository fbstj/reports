<?php
namespace entries;
require_once 'lib/db.php';

function from_report($id)
{
    return \db\get_children($id, 'entry');
}

function get($id, $deep=false)
{
    $e = \db\get_claim($id);
    if ($e->type != 'entry')
        return null;
    $urls = \db\get_children($id, 'url');
    $e->url = $urls[0];
    if ($deep)
        $e->url = \urls\get($e->url);
    $dates = \db\get_children($id, 'date');
    if (count($dates))
    {
        $e->date = $dates[0];
        if ($deep)
            $e->date = \dates\get($e->date);
    }
    $note = \db\get_children($id, 'note');
    if (count($note))
    {
        $e->note = $note[0];
        if ($deep)
            $e->note = \db\get_claim($e->note);
    }
    return $e;
}

function search($query)
{   // TODO: entries should have a text-search enabled child-claim
    $WHERE = "`type` = 'entry' AND `body` LIKE ?";
    return \db\select_column(\db\COL_ID, $WHERE, ["%{$query}%"]);
}

function show($e_id, $opts = null)
{
    $entry = \entries\get($e_id, true);
    ob_start();
?>
    <blockquote>
    <?=$entry->body?>
    </blockquote>
<?php
    if ($entry->note)
    {
?>
    <div style="font-size: small;"><b>Notes:</b> <?=$entry->note->body?></div>
<?php
    }
    return ob_get_clean();
}

function get_links($e_id)
{
    $links = [];
    foreach (\urls\_list($e_id) as $u)
        $links[] = $u->show('source');
    $d = \dates\get_from_parent($e_id);
    if (!is_null($d))
        $links['date'] = $d->show();
    foreach (\urls\_generate($e_id, 'entry') as $k => $u)
    {
        $links[$k] = $u->show($k);
    }
    return $links;
}
