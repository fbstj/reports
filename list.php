<?php
require_once 'lib/db.php';
?>
<meta charset="utf-8">
<link rel="stylesheet" href="./style.css" type="text/css" />
<title>List all reports</title>
<?php require_once 'lib/nav.inc.php'; ?>
<header>
    <h1>Reports</h1>
</header>

<ul>
<?php

require_once 'lib/reports.php';
require_once 'lib/dates.php';
foreach (\reports\all() as $report)
{
    $entries = \db\get_children($report->id, 'entry');
?>
<li>
    <a href="./show.php?r=<?=$report->id?>"><?=$report->body?></a>
    - <i><?=$report->date->show()?></i>
     - <?=count($entries)?>
    <small><?php
        ?>[<a href="./edit.php?type=report&id=<?=$report->id?>&list">edit</a>]<?php
    ?></small>
</li>
<?php
}

?>
</ul>
<a href="./add.php?report">Add a report</a>