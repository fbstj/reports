<?php
require_once 'lib/db.php';

if (!isset($_GET['id']))
{   // redirect to ./list.php
    header('Location: ./list.php');
    die();
}

$claim = \db\get_claim($_GET['id']);

if ($claim->type == 'report')
{   // redirect to ./show.php
    header('Location: ./show.php?r='.$claim->id);
    die();
}
if ($claim->type == 'entry')
{   // redirect to ./list.php
    header('Location: ./show.php?r='.$claim->parent.'#'.$claim->id);
    die();
}

var_dump($claim);
